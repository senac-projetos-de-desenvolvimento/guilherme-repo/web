# Front End da Aplicação ConstruObre

## Apropriar do Projeto

git clone https://gitlab.com/projeto-de-desenvolvimento-2020.2/guilherme-repo/web.git

## Dependências para Instalar

* npm install -g @angular/cli

## Rodar a Aplicação

* npm install
* ng serve

## Deploy

* -------

# Routes Principais

* /home -- Home da Aplicação
* /registro -- Tela de cadastro
* /login -- Tela para realizar login
* /cadCliente -- Area administrativa, para realizar o cadastro de clientes
